# Agile Accelerator Setup for Project Leads
## Setup your Scrum Team
### Create Team
- [ ] Click the 'Teams' tab and create a new record for your team.
- [ ] Add your Team Members and their allocations.
- [ ] Add a Product Tag code for your project. This tag is used to assign work to the Scrum Team. 

>*Known issue:* 
The same person cannot be allocated over 100% across Teams. 
Sprint Team allocation for each user can be viewed and edited from the User record page.
If setting up a new project ahead of a resources roll off, then assign them to your project initially at 0%. 
Project Leads will need to zero out or remove Team Member records when rolling resources off.

#### Out of Office
Upcoming leave records can be created for your Team Members from the Out Of Office Logs. This will be tracked against your Sprint Team for visibility and will also reduce the Team Member's availability in the relevant Sprint planning wall.

#### External Users
External users can be added from the 'Edit' Team Member screen only (don't ask me why!). Create a dummy internal user then Edit the record. The 'Customer Portal Users' selection option is then available.

## Product Tags

- [ ] Create a Product Tag for each workstream within your project. For example, `Pardot` & `Sales`

Product tags are used to assign work to the Scrum Team and can be used in conjunction with Assignment Rules. Product Tags would typically represent a workstream or sub-team within a Project.

### Product Tag Fields

A field set in the Agile Accelerator can be used to show/hide custom fields per Product Tag. This can be used to control field visibility across teams and projects upon request. 

## Epics

An Epic is a large User Story that can’t be completed in a single sprint or sometimes even multiple sprints.
As a best practice, Design Leads and technical design documents/specifications can be tracked against each Epic using Files & Chatter.

## Sprints

- [ ] Create one or more upcoming Sprints based on your project plan & sprint cadence.

*Tips:*
- Sprint start & end dates cannot be changed once the Sprint has started so do not 'lock' a Sprint in until you're sure on your sprint cadence.
- Hours in a Work Day can be used to tailor how capacity is managed when assigning work in your Sprint planning wall. Typically this is set to 6 to allow time for meetings, emails, etc.
- Each sprint can contain a Sprint Goal & track any risks which are being managed against this goal.

## Work

User Stories & Bugs can be created as Work items. These can then be story pointed and prioritised either prior to dataload or in the Work Manager.

From the Work Manager you can also start to assign your stories to Sprints.

Each work item can have an Assignee and a QA Engineer to track who is managing the build & testing.
